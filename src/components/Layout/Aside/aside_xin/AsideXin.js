import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LigXinmu from '../../../List/lig_xinmu/LigXinmu';
// import './css.scss';

class AsideXin extends Component {
    constructor(props) {
        super(props);
    }
    static propTypes = {
        list: PropTypes.array,
    };
    static defaultProps = {
        list: [],
    };
    render() {
        return this.props.list ? (
            <React.Fragment>
                <LigXinmu list={this.props.list} />
            </React.Fragment>
        ) : '';
    }
}

export default AsideXin;

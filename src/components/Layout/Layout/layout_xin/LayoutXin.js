import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './css.scss';

class LayoutXin extends Component {
    static propTypes = {
        header: PropTypes.node.isRequired,
        children: PropTypes.node,
        aside: PropTypes.node,
        footer: PropTypes.node.isRequired,
    }
    render() {
        return (
            <div className="layout_xin">
                {this.props.header}
                <main>
                    <div className="container">
                        {this.props.aside}
                        <div className="content">
                            {this.props.children}
                        </div>
                    </div>
                </main>
                {this.props.footer}
            </div>
        );
    }
};

export default LayoutXin;

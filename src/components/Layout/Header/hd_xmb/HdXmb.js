import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Stxin from '../../../Form/Select/St_xin/StXin';
import LigXinmu from '../../../List/lig_xinmu/LigXinmu';
import BtXin from '../../../Button/bt_xin/BtXin.js';
import ClXmb from '../../../Carousel/cl_xmb/ClXmb';
import IcXin from '../../../Icon/ic_xin/IcXin';
import ClickOutside from '../../../Utils/ClickOutside';

import './css.scss';

class HdXmb extends Component {
    constructor(props) {
        super(props);
        this.state = {
            xinMode: false,
            memberMode: false,
        };
    }
    static propTypes = {
        isLogin: PropTypes.object,
        logo: PropTypes.object,
        memberMenu: PropTypes.array,
        xinMenu: PropTypes.array,
    };
    static defaultProps = {
        isLogin: {
            login: false,
            href: '#',
            name: '',
            data: {
                imgSrc: '',
                link: '#',
                target: '',
            },
        },
    };
    toggleXinMenu = () => {
        this.setState((prevState) => ({
            xinMode: !this.state.xinMode,
            memberMode: false,
        }));
    }
    toggleMemberMenu = () => {
        this.setState((prevState) => ({
            memberMode: !this.state.memberMode,
            xinMode: false,
        }));
    }
    render() {
        const { isLogin, logo, memberMenu, xinMenu } = this.props;
        const style = cx('hd_xmb');
        const hamburgerBtn =
            this.state.xinMode ? 'ic-cross' : 'ic-menu';
        const memberdata = isLogin.data;
        return (
            <header className={style}>
                <div className="container">
                    <div className='logo'><ClXmb {...logo} /></div>
                    <div className='hd'>
                        <div className='lhd'>
                            {
                                xinMenu &&
                            <ClickOutside
                                onClickOutside={() => {
                                    this.setState( {
                                        xinMode: false,
                                    });
                                }}
                            >
                                <Stxin mode={this.state.xinMode}
                                    titleMode={
                                        <IcXin
                                            name={hamburgerBtn}
                                            size="x2"
                                            onClick={this.toggleXinMenu}
                                        />
                                    }
                                    content={<LigXinmu list={xinMenu} />}
                                />
                            </ClickOutside>
                            }
                        </div>
                        <div className='rhd'>
                            {
                                !isLogin.login ?
                                    <BtXin
                                        text={'登入'}
                                        type="linkbutton"
                                        link={isLogin.href}
                                        className='bd_red'
                                    />
                                    :
                                    (
                                        <div className='mbinfo'>
                                            <BtXin
                                                text={isLogin.name}
                                                link={'#'}
                                                className='bd_none'
                                            />
                                            <ClXmb {...memberdata} />
                                            {
                                                memberMenu &&
                                            <ClickOutside
                                                className='mbmu'
                                                onClickOutside={() => {
                                                    this.setState( {
                                                        memberMode: false,
                                                    });
                                                }}
                                            >
                                                <Stxin mode={this.state.memberMode}
                                                    titleMode={
                                                        <IcXin
                                                            name="ic-arrow-down"
                                                            size="x2"
                                                            onClick={this.toggleMemberMenu}
                                                        />
                                                    }
                                                    content={<LigXinmu list={memberMenu} />}
                                                />
                                            </ClickOutside>
                                            }
                                        </div>
                                    )
                            }
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}

export default HdXmb;

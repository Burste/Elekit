import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import ClXmb from '../../../Carousel/cl_xmb/ClXmb';
import ThXin from '../../../Title/th_xin/ThXin';
import BtXin from '../../../Button/bt_xin/BtXin.js';
import './css.scss';

class FtXin extends Component {
    static propTypes = {
        logo: PropTypes.object,
        fmenu: PropTypes.array,
    };
    static defaultProps = {

    };

    renderMenus = (menu, i) => {
        const arr = [];
        arr.push(
            <li key={'ulmu' + i}>
                <ThXin
                    text={menu.menu_title}
                    // className='sm'
                />
            </li>
        );
        menu.subMenu && menu.subMenu.forEach((item, i) => {
            arr.push(
                <li key={'sub' + i}>
                    <BtXin
                        text={item.title}
                        type="linkButton"
                        link={item.link}
                        target={item.target}
                        className='bd_none'
                    />
                </li>
            );
        });
        return arr;
    }
    render() {
        const { logo, fmenu } = this.props;
        const style = cx('ft_xin');
        return (
            <footer className={style}>
                <div className="container">
                    <ClXmb {...logo} />
                    {
                        fmenu &&
                    <nav>
                        {
                            fmenu.map((item, i) => {
                                return (
                                    <ul key={'mu' + i}>
                                        {this.renderMenus(item, i)}
                                    </ul>
                                );
                            })
                        }
                    </nav>
                    }
                    <address>
                    台灣台北市114內湖區石潭路151號<br />
                    COPYRIGHT © XINMEDIA.COM ALL RIGHTS RESERVED
                    </address>
                </div>
            </footer>
        );
    }
}

export default FtXin;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './css.scss';

class StXin extends Component {
    static propTypes = {
        mode: PropTypes.bool,
        className: PropTypes.string,
        content: PropTypes.object,
        toggleContent: PropTypes.func,
        titleMode: PropTypes.oneOfType(
            [PropTypes.element, PropTypes.string]
        ),
    };
    constructor(props) {
        super(props);
        this.state = {
            mode: false,
        };
    }

    render() {
        const { className, mode, content, titleMode } = this.props;
        const style = cx('st_xin', className, { pcshow: mode });
        return (
            <div className={style}>
                {titleMode}
                <div className="childcontent">
                    {content}
                </div>
            </div>
        );
    }
}

export default StXin;

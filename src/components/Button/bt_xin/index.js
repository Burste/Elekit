import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import { addNum } from '../../../redux/actions';
import './css.scss';

class BtXin extends Component {
    constructor(props) {
        super(props);
    }
    static propTypes = {
        text: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object,
        ]),
        type: PropTypes.string,
        onClick: PropTypes.func,
        link: PropTypes.string,
        target: PropTypes.string,
        className: PropTypes.string,
    };
    static defaultProps = {
        type: '123',
        text: '',
        className: '',
    };
    handleOnClick() {
        const { onClick } = this.props;
        onClick && onClick();
    }
    render() {
        const {
            type,
            text,
            link,
            target,
            className,
        } = this.props;
        {
            switch (type) {
                case 'linkButton':
                    return (
                        <a className={`bt_xin ${className}`}
                            href={link}
                            rel="noopener noreferrer"
                            target={target}>{text}
                        </a>
                    );
                    break;
                default:
                    return (
                        <div className={`bt_xin ${className}`}
                            onClick={() => this.handleOnClick()}>{text}</div>
                    );
            }
        }
    }
}
const mapStateToProps = (state) => {
    return {
        total: state.total,
        error: state.error,
    };
};
// const mapDispatchToProps = (dispatch) => {
//     return {
//         total:
//             dispatch(addNum()),
//     };
// };
BtXin = connect(mapStateToProps)(BtXin);

export default BtXin;

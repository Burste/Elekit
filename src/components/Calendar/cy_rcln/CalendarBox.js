import React, { PureComponent } from 'react';
import cx from 'classnames';
import Proptypes from 'prop-types';
import { getYearAndMonth } from '../../../scripts/utilities/date';
import './css.scss';
const Week = () => (
    <div className="week">
        <span className="sun holiday">日</span>
        <span className="mon">一</span>
        <span className="tue">二</span>
        <span className="wed">三</span>
        <span className="thu">四</span>
        <span className="fri">五</span>
        <span className="sat holiday">六</span>
    </div>
);

const WeekRow = ({
    dayArray,
    onDateClick,
}) => {
    return (
        <div className="week_row">
            {
                dayArray.map((v, i) => {
                    if (typeof v === 'undefined') return <EmptyDay key={`empty${i}`} />;
                    return (
                        <Day
                            key={v.date}
                            date={v.date}
                            onClick={onDateClick}
                            active={v.active}
                            txt={v.txt}
                            isBetween={v.isBetween}
                            isStart={v.isStart}
                            isEnd={v.isEnd}
                            isDisabled={v.isDisabled}
                            isHover={v.isHover}
                            setHoverDate={v.setHoverDate}
                        />
                    );
                })
            }
        </div>
    );
};


const YearMonth = ({
    year,
    month,
    changeYear,
    changeMonth,
    isMobile,
}) => {
    return (
        <div className="year_month">
            <p className="title custom-select">
                {changeYear ? <span className="select-selected"
onClick={changeYear}>{year}年</span> : `${year}年`}
                {changeMonth ? <span className="select-selected"
onClick={changeMonth}>{month}月</span> : `${month}月`}
            </p>
            {
                isMobile ? null : <Week />
            }
        </div>
    );
};

const EmptyDay = () => (
    <div className="date empty"></div>
);

const Day = ({
    isBetween,
    date,
    txt = '',
    active = false,
    isStart = false,
    isEnd = false,
    isDisabled = false,
    isHover = false,
    onClick = (date) => {
console.log(date);
},
    onMouseEnter = () => {},
}) => {
    const dateStr = date.toISOString();
    // 去除0
    const dateVal = dateStr.slice(0, 10);
    const showday = dateStr.slice(8, 10).replace(/0+(\d)/gi, '$1');
    const d = new Date();
    const today = `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`;
    return (
        <div
            className={cx('date', {
                active: active,
                isBetween: isBetween,
                startDay: isStart,
                endDay: isEnd,
                disabled: isDisabled,
                isHover: isHover,
            })}
            onClick={() => {
                if (isDisabled) return;
                onClick(dateVal);
            }}
            onMouseEnter={() => {
                if (isDisabled) return;
                onMouseEnter(dateVal);
            }}
        >
            <div className={`date_box ${today === dateVal ? 'today' : ''}`}>
                <span className="date_num">{showday}</span>
                <span className="txt">{txt}</span>
            </div>
        </div>
    );
};

class CalendarBox extends PureComponent {
    static defaultProps = {
        selectedStartDate: '',
        selectedEndDate: '',
        startTxt: '去程',
        endTxt: '回程',
        startDate: null,
        endDate: null,
        isMobile: false,
        setHoverDate: () => {},
        onDateClick: () => {},
    };

    static propTypes = {
        isMobile: Proptypes.bool,
        onDateClick: Proptypes.func,
    };

    constructor() {
        super();
        this.state = {
            openYear: false,
            openMonth: false,
        };
    }

    calcDayArray() {
        const {
            selectedStartDate,
            selectedEndDate,
            startTxt,
            endTxt,
            startDate,
            endDate,
            doubleChoose,
            startMonth,
        } = this.props;

        // 該月第一天是禮拜幾
        const firstDay = startMonth.getDay();
        const [year, month] = getYearAndMonth(startMonth.toISOString().slice(0, 7));
        const date = new Date(year, month, 0);
        const daysLength = date.getDate();
        const minDate = new Date(startDate).getTime(); // 最小可選日
        const maxDate = new Date(endDate).getTime(); // 最大可選日
        const selectStart = new Date(selectedStartDate).getTime(); // 已選出發日
        const selectedEnd = new Date(selectedEndDate).getTime(); // 已選結束日
        const hasStartAndEnd = selectedStartDate.length > 0 && selectedEndDate.length > 0;
        const result = [...new Array(daysLength)].map((v, i) => {
            const thisDay = new Date(year, month - 1, i + 1, 8);
            const thisTime = thisDay.getTime();
            const isStart = thisTime === selectStart;
            const isEnd = thisTime === selectedEnd;
            const isBetween = doubleChoose && thisTime > selectStart && thisTime < selectedEnd;
            const dateObj = {
                date: thisDay,
                active: isStart || isEnd,
                txt: isStart ?
                    startTxt :
                    isEnd ? endTxt : '',
                isBetween,
                isStart: hasStartAndEnd && isStart, // 兩天都選取才加
                isEnd: hasStartAndEnd && isEnd,
                isDisabled: (startDate && thisTime < minDate) ||
                    (endDate && thisTime > maxDate),
            };

            return dateObj;
        });

        // firDay前面補空日期, 補滿成7天
        result.unshift(...[...new Array(firstDay)]);
        // 計算陣列離42還差幾天
        const lastDay = 42 - result.length;
        result.push(...[...new Array(lastDay)]);

        return result;
    }

    calcWeekRow() {
        const dayArray = this.calcDayArray();
        const weekRowArray = [];

        for (let i = 0; i < 6; i++) {
            weekRowArray.push(dayArray.splice(0, 7));
        }

        return weekRowArray;
    }

    // get Year
    getYear = (year) => {
        const { changeYear } = this.props;
        this.setState({ openYear: false });
        changeYear && changeYear(year);
    };
    // render Year option
    renderSelectYear = () => {
        const { startMonth, activeStart, activeEnd } = this.props;
        const selectMonth = startMonth.getMonth() + 1;
        const min = activeStart.split('-');
        const max = activeEnd.split('-');
        const range1 = selectMonth >= min[1] ? min[0] : parseInt(min[0]) + 1;
        const range2 = selectMonth <= max[1] ? max[0] : parseInt(max[0]) - 1;

        const yearOption = [];
        for (let i = parseInt(range1); i <= parseInt(range2); i++) {
            yearOption.unshift(
                <li
                    className={`${i === startMonth.getFullYear() ? 'active' : ''}`}
                    onClick={() => this.getYear(i)}
                    key={Date.now() + i}
                    value={i}>{i}年
                </li>);
        }
        return yearOption;
    };
    yearSelect() {
        return (
            <ul className={`year-content`}>
                {this.renderSelectYear()}
            </ul>
        );
    }
    // get Month
    getMonth = (month) => {
        const { changeMonth } = this.props;
        this.setState({ openMonth: false });
        const date = month < 10 ? `0${month}` : month;
        changeMonth && changeMonth(date);
    };
    // render Month
    renderSelectMonth = () => {
        const { startMonth, activeStart, activeEnd } = this.props;
        const month = startMonth.getMonth() + 1;
        const selectYear = `${startMonth.getFullYear()}`;
        const min = activeStart.split('-');
        const max = activeEnd.split('-');
        const range1 = selectYear === min[0] ? min[1] : 1;
        const range2 = selectYear === max[0] ? max[1] : 12;

        const monthOption = [];
        for (let i = parseInt(range1); i <= parseInt(range2); i++) {
            console.log(i);
            monthOption.push(
                <li
                    className={`${i === month ? 'active' : ''}`}
                    onClick={() => this.getMonth(i)}
                    key={Date.now() + i}
                    value={i}>{i}月
                </li>
            );
        }
        return monthOption;
    };
    monthSelect() {
        return (
            <ul className={`month-content`}>
                {this.renderSelectMonth()}
            </ul>
        );
    }
    render() {
        const { openYear, openMonth } = this.state;

        const {
            onDateClick,
            setHoverDate,
            isMobile,
            startMonth,
            changeYear,
            changeMonth,
        } = this.props;

        const weekRow = this.calcWeekRow();

        return (
            <div className="calendar_box">
                {openYear ? this.yearSelect() : null}
                {openMonth ? this.monthSelect() : null}
                <YearMonth
                    year={startMonth.getFullYear()}
                    month={startMonth.getMonth() + 1}
                    isMobile={isMobile}
                    changeYear={changeYear ? () => this.setState({ openYear: true }) : false}
                    changeMonth={changeMonth ? () => this.setState({ openMonth: true }) : false}
                />
                <div
                    className="month_box"
                    onMouseLeave={() => {
setHoverDate(null);
}}
                >
                    {
                        weekRow.map((v, i) => (
                            <WeekRow
                                key={`week${i}`}
                                dayArray={v}
                                onDateClick={onDateClick}
                            />
                        ))
                    }
                </div>
            </div>
        );
    }
}

export { Week };
export default CalendarBox;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './css.scss';

class ClXmb extends Component {
    static propTypes = {
        imgSrc: PropTypes.string,
        link: PropTypes.string,
        target: PropTypes.string,
    };
    static defaultProps = {
        imgSrc: '',
        link: '#',
    };
    render() {
        const {
            imgSrc,
            link,
            target,
        } = this.props;
        return link ? (
            <a className="cl_xmb"
                href={link}
                target={target}>
                <img src={imgSrc} />
            </a>
        ) : (<div className="cl_xmb"
            href={link}
            target={target}>
            <img src={imgSrc} />
        </div>);
    }
}

export default ClXmb;

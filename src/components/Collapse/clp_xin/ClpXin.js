import React, { Component } from 'react';
import './css.scss';
import PropTypes from 'prop-types';
const ClpXin = (
    WrappedComponent,
    data
) => {
    class HOC extends Component {
        constructor(props) {
            super(props);
        }
        static propTypes = {
            defaultStyle: PropTypes.object,
            WrappedClassName: PropTypes.string,
            listType: PropTypes.string,
        };
        static defaultProps = {
            defaultStyle: {
                type: 'checkbox',
                className: 'titlewhite',
                inputClassName: '',
                labelClassName: 'white',
            },
            WrappedClassName: '',
        };
        render() {
            const { className, type, inputClassName, labelClassName } = this.props.defaultStyle;
            const { WrappedClassName, listType } = this.props;
            return (
                <div className={`clp_xin default ${className}`}> {
                    Object.values(data).map((ele, idx) => (
                        <div className="tab"
                            key={idx} >
                            <input id={`${type}tab_${inputClassName}_${idx}`}
                                name={'tabs'}
                                type={type}
                                className={`int_xin ${inputClassName}`} />
                            <label htmlFor={`${type}tab_${inputClassName}_${idx}`}
                                title={ele.menu_title}
                                className={`${labelClassName}`}>{ele.menu_title}
                            </label>
                            <div className="tab-content" >
                                <WrappedComponent {...ele}
                                    listType={listType}
                                    className={WrappedClassName} />
                            </div>
                        </div>
                    ))}
                </div>
            );
        }
    }
    return HOC;
};


export default ClpXin;

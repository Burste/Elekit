import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import BtXin from '../../Button/bt_xin/BtXin';
import './css.scss';

class TbXin extends Component {
    static propTypes = {
        classname: PropTypes.string,
        titleData: PropTypes.array,
        contentData: PropTypes.array,

        // newClassname: PropTypes.string,
        flexTitleData: PropTypes.array,
        flexContentData: PropTypes.array,
    };


    createTitle(dataArr, callback) {
        const arr = [];
        dataArr.length &&
            dataArr.forEach((obj, index) => {
                callback && callback(item);
                arr.push(
                    <div className="text"
                        key={`t_${index}`}>
                        {obj.text}
                    </div>
                );
            });
        return arr;
    }

    createNewTitle(dataArr, callback) {
        const arr = [];
        dataArr.length &&
            dataArr.forEach((obj, index) => {
                callback && callback(item);
                arr.push(
                    <div className="text"
                        key={`t_${index}`}>
                        {obj.text}
                    </div>
                );
            });
        return arr;
    }

    createContent() {
        const { contentData, titleData } = this.props;
        const arr = [];
        contentData.length &&
            contentData.forEach((item, index) => {
                arr.push(
                    <div
                        className="row content"
                        key={`${item.orderId}_${index}`}
                    >
                        <div
                            className="text date"
                            data-title={titleData[0].text}
                        >
                            {item.date}
                        </div>
                        <div className="text"
                            data-title={titleData[1].text}>
                            {item.orderId === '-' ? (
                                <BtXin
                                    className="bd_none"
                                    type='linkButton'
                                    link={item.orderLink}
                                    text={item.orderId}
                                    onClick={this.toggleContent}
                                />

                            ) : (
                                '-'
                            )}
                        </div>
                        <div className="text"
                            data-title={titleData[2].text}>
                            <BtXin
                                type='linkButton'
                                link={item.sourceLink}
                                text={item.sourceTitle}
                            />
                        </div>
                        <div className="text"
                            data-title={titleData[3].text}>
                            {item.platform}
                        </div>
                        <div className="text"
                            data-title={titleData[4].text}>
                            {item.pointStatus}
                        </div>
                    </div>
                );
            });
        return arr;
    }

    createShopContent() {
        const { shopContentData, flexTitleData } = this.props;
        const titleValueArr = [];
        for (let i = 0; i < flexTitleData.length; i ++) {
            titleValueArr.push(flexTitleData[i]['value']);
        }
        const arr = [];
        // console.log('flexTitleData')
        // console.log(flexTitleData)
        // console.log('titleValueArr')
        // console.log(titleValueArr)
        // console.log('shopContentData')
        // console.log(shopContentData[0]['orderno'])
        shopContentData.length &&
        shopContentData.forEach((item, index) => {  // 資料比數(有幾筆，產幾筆(列))
            arr.push(
                <div
                    className="row content"
                    key={`${item.orderId}_${index}`}
                >
                    <div className="text"
                            data-title={flexTitleData[0].text}
                            dangerouslySetInnerHTML = {{__html:shopContentData[index]['orderno']}}
            >  
            {/* {item.orderno} */}
            {/* <p dangerouslySetInnerHTML = {{__html:shopContentData[index]['orderno']}}> */}

            {/* </p> */}
            {/* 這邊已經用innerHTML就不能再包 {item.orderno}了 */}
                    </div>
                    <div className="text"
                            data-title={flexTitleData[1].text}
                            >
                            {item.date}
                    </div>
                    <div className="text"
                            data-title={flexTitleData[2].text}>
                        {item.status}
                        </div>
                        <div className="text"
                            data-title={'總計'}>
                            <div className='log'>
                                <div className='logname'>{item.log[0].name}:&nbsp;</div>
                                <div className='logqty'>{item.log[0].qty}</div>
                            </div>
                            <div className='log'>
                                <div className='logname'>{item.log[1].name}:&nbsp;</div>
                                <div className='logqty'>{item.log[1].qty}</div>
                            
                            </div>
                            <div className='log'>
                                <div className='logname'>{item.log[2].name}:&nbsp;</div>
                                <div className='logqty'>{item.log[2].qty}</div>
                            
                            </div>
                        </div>
                 
                 </div>
            );
        });
        return arr;
    }

    createNewContent() {
        const { flexContentData, flexTitleData } = this.props;
        const titleValueArr = [];
        for (let i = 0; i < flexTitleData.length; i ++) {
            titleValueArr.push(flexTitleData[i]['value']);
        }
        const arr = [];
        flexContentData.length &&
        flexContentData.forEach((item, index) => {  // 資料比數(有幾筆，產幾筆(列))
            arr.push(
                <div
                    className="row content"
                    key={`${item.orderId}_${index}`}
                >
                {this.insideContent(titleValueArr, index)}
                 </div>
            );
        });
        return arr;
    }

    insideContent(titleValueArr,index) {
        const { flexContentData, flexTitleData } = this.props;
        const lenFlexContentData = flexContentData.length;
        const flexContentArr = []   //原本寫死的Arr改成用迴圈
        const newTitleValueArr = [];
        const newTitleTextArr = [];
        for (let i = 0; i < flexTitleData.length; i ++) {
            newTitleValueArr.push(flexTitleData[i]['value']);
            newTitleTextArr.push(flexTitleData[i]['text']);
        }
        for (let j =0; j<newTitleValueArr.length; j++) {
            flexContentArr.push(
                <div className="text" key={`index${j}`}
                        data-title={[newTitleTextArr[j]]}
                        // dangerouslySetInnerHTML={flexContentData[index][newTitleValueArr[j]]}
                        dangerouslySetInnerHTML = {{__html:flexContentData[index][newTitleValueArr[j]]
                        }}
>
                    </div>
            )
        }
        return flexContentArr
    }


    render() {
        const { titleData, classname, flexTitleData, flexContentData } = this.props;
        const style = cx('tb_xin', classname);
        // console.log()
        // 舊的TbXin給旅遊金用的
        if (titleData) {
            return (
                <div className={style}>
                    <div className="row head">{this.createTitle(titleData)}</div>
                    <div className="body">{this.createContent()}</div>
                </div>
            );
        } else if (flexContentData){ // 新的給order用的
            return (
                <div className={style}>
                    <div className="row head">{this.createNewTitle(flexTitleData)}</div>
                    <div className="body">{this.createNewContent()}</div>
                </div>
            );
        } else {  // 專給欣嚴選用的
            return (
                <div className={style}>
                    <div className="row head">{this.createNewTitle(flexTitleData)}</div>
                    {/* <div className="body">{this.createNewContent()}</div> */}
                    <div className="body">{this.createShopContent()}</div>
                </div>
            );
        }
    }
}

export default TbXin;

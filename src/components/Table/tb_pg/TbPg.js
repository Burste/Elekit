import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import PgXin from '../../Pagination/pg_xin/PgXin';
import TbXin from '../tb_xin/TbXin';
import './css.scss';

class TbPg extends Component {
    static propTypes = {
        // TbXin
        classname: PropTypes.string,
        titleData: PropTypes.array,
        contentData: PropTypes.array,
        // PgXin
        defaultPage: PropTypes.number,
        defaultPageSize: PropTypes.number,
        pageSize: PropTypes.number,
        onChange: PropTypes.func,
        total: PropTypes.number,
    };

    render() {
        const { titleData, contentData, classname, defaultPage, pageSize, total, onChange } = this.props;
        const style = cx('tb_pg', classname);
        return (
            <div className={style}>
                <TbXin
                    titleData={titleData}
                    contentData={contentData}
                    pageSize={pageSize}
                />
                <PgXin
                    defaultPage={defaultPage} // 預設頁
                    pageSize={pageSize} // 頁面呈現數量
                    total={total} // 資料數量
                    onChange={onChange} // 換頁變更觸發
                />
            </div>
        );
    }
}

export default TbPg;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import IntXin from '../../Form/Input/int_xin/IntXin';
import BtXin from '../../Button/bt_xin/BtXin';
import IcXin from '../../Icon/ic_xin/IcXin';
import KEYCODE from '../../../scripts/utilities/KeyCode';
import './css.scss';
function isInteger(value) {
    return typeof value === 'number' &&
      isFinite(value) &&
      Math.floor(value) === value;
}
function calculatePage(p, state, props) {
    let pageSize = p;
    if (typeof pageSize === 'undefined') {
        pageSize = state.pageSize;
    }
    return Math.floor((props.total - 1) / pageSize) + 1;
}
class PgXin extends Component {
    static propTypes = {
        defaultPage: PropTypes.number,
        defaultPageSize: PropTypes.number,
        pageSize: PropTypes.number,
        className: PropTypes.string,
        onChange: PropTypes.func,
        total: PropTypes.number,
    };
    static defaultProps = {
        defaultPage: 1,
        pageSize: 10,
        defaultPageSize: 10,
    };

    constructor(props) {
        super(props);
        this.state = {
            currentPage: props.defaultPage,
            currentInputValue: props.defaultPage,
            pageSize: props.pageSize || props.defaultPageSize,
        };
        this.isPrevEnd = false;
        this.isNextEnd = false;
        this.handleKeyUp = this.handleKeyUp.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleKeyUp= (e) => {
        const inputValue = e.target.value;
        const { currentInputValue } = this.state;
        let value = null;
        if (inputValue === '') {
            value = inputValue;
        } else if (isNaN(Number(inputValue))) {
            value = currentInputValue;
        } else {
            value = Number(inputValue);
        }

        if (value !== currentInputValue) {
            this.setState({
                currentInputValue: value,
            });
        }

        if (e.keyCode === KEYCODE.ENTER) {
            this.handleChange(value);
        } else if (e.keyCode === KEYCODE.ARROW_UP) {
            this.handleChange(value - 1);
        } else if (e.keyCode === KEYCODE.ARROW_DOWN) {
            this.handleChange(value + 1);
        }
    }
    handleChange = (num) => {
        let page = num;
        if (this.isValid(page)) {
            const allPage =
            calculatePage(undefined, this.state, this.props);
            if (page > allPage) {
                page = allPage;
            }

            if (!('current' in this.props)) {
                this.setState({
                    currentPage: page,
                    currentInputValue: page,
                });
            }

            const pageSize = this.state.pageSize;
            this.props.onChange(page, pageSize);

            return page;
        }

        return this.state.current;
    }

    hasPrev = () => {
        return this.state.currentPage > 1;
    }

    hasNext = () => {
        const { currentPage } = this.state;
        return currentPage <
               calculatePage(undefined, this.state, this.props);
    }
    prev = () => {
        if (this.hasPrev()) {
            this.handleChange(this.state.currentPage - 1);
        } else {
            this.isPrevEnd = true;
        }
    }

    next = () => {
        if (this.hasNext()) {
            this.handleChange(this.state.currentPage + 1);
        }
    }
    isValid = (page) => {
        return isInteger(page) && page >= 1 && page !== this.state.currentPage;
    }
    render() {
        const { className } = this.props;
        const { currentInputValue } = this.state;
        const style = cx('pg_xin', className);
        const allPages = calculatePage(undefined, this.state, this.props);
        return (
            <div className={style}>
                <BtXin className="btn prev"
                    onClick={this.prev}
                    text={
                        <IcXin className="icon-prev"
                            name="ic-arrow-down"></IcXin>
                    }
                />
                <div className="simple-pager">
                    <IntXin
                        type='input'
                        placeholder="頁數"
                        value={currentInputValue}
                        onChange={this.handleKeyUp}
                        onKeyUp={this.handleKeyUp}
                    />
                    <span className="pager-detail">頁/共 {allPages} 頁</span>
                </div>
                <BtXin className="btn prev"
                    onClick={this.next}
                    text={
                        <IcXin className="icon-next"
                            name="ic-arrow-down"></IcXin>
                    }
                />
                <BtXin className="btn bg_red turnPage"
                    text="跳頁"
                    onClick={()=>this.handleChange(currentInputValue)}
                />
            </div>
        );
    }
}

export default PgXin;

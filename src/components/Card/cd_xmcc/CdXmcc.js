import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './css.scss';

class CdXmcc extends Component {
    constructor(props) {
        super(props);
    }
    static propTypes = {
        description: PropTypes.string,
        className: PropTypes.string,
        subComponent: PropTypes.func,
    };
    static defaultProps = {
        description: null,
        className: '',
    };
    render() {
        const { description, className } = this.props;
        return (
            <div className={`cd_xmcc ${className}`}>
                {description && <p>{description}</p>}
                {this.props.subComponent && this.props.subComponent(this.state)}
            </div>
        );
    }
}

export default CdXmcc;

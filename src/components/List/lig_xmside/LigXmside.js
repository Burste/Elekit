import React, { Component } from 'react';
import PropTypes from 'prop-types';
import BtXin from './../../Button/bt_xin/BtXin.js';
import './../../Button/bt_xin/css.scss';
import './css.scss';

// Reader Props SubComponent
class LigXmside extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    static propTypes = {
        className: PropTypes.string,
        subMenu: PropTypes.array,
        onClick: PropTypes.func,
    };
    static defaultProps = {
        subMenu: [],
        className: '',
    };
    onClick=(e)=> {
        this.props.onClick && this.props.onClick(e);
        console.log('onClick');
    }
    render() {
        const { subMenu, className } = this.props;
        return (
            <div className={`lig_xmside btn ${className}`} >
                {subMenu.map((item, idx) => (
                    <BtXin
                        {...item}
                        text={item.title}
                        onClick={(e) => this.onClick(e)}
                        key={`${item.title} ${idx}`}
                    />
                ))}
            </div>
        );
    }
}

export default LigXmside;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import BtXin from './../../Button/bt_xin/BtXin.js';
import cx from 'classnames';
import './../../Button/bt_xin/css.scss';
import './css.scss';

class LigXinmu extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    static propTypes = {
        list: PropTypes.array,
        subMenu: PropTypes.array,
        listType: PropTypes.string,
    };
    static defaultProps = {
        listType: 'button',
    }
    render() {
        const { list, listType, subMenu } = this.props;
        const style = cx('lig_xinmu');
        if (list && list.length > 0 || subMenu && subMenu.length) {
            switch (listType) {
                case 'pureText':
                    console.log('pureTEXT');
                    return (<div className={style}><p>{subMenu.map((item, i) => item.text)}</p></div>);
                default:
                    return (
                        <ul className={style}>
                            {list.map((item, i) => {
                                return (
                                    <li key={'xmu' + i}>
                                        <BtXin
                                            text={item.title}
                                            type="button"
                                            link=""
                                            className="bd_none"
                                        />
                                    </li>
                                );
                            })}
                        </ul>
                    );
            }
        }
        return '';
    }
}

export default LigXinmu;

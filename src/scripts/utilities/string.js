
export function toQueryString(data) {
    const option = data;
    let string = '';
    const optionLength = Object.keys(option).length;
    let i = 0;
    for (const key in option) {
        if ((Object.prototype.hasOwnProperty.call(option, key))) {
            i++;
            if (i >= optionLength) {
                string += key + '=' + option[key];
            } else {
                string += key + '=' + option[key] + '&';
            }
        }
    }
    return string;
}

export const formatPhoneCode = (data)=> {
    const arr = [];
    data && data.forEach((item) => {
        arr.push({ text: `（${item.phoneCode}）${item.countryName}`, value: item.phoneCode.substr(1) });
    });
    return arr;
};
export const formatToOptions = (data) => {
    const arr = [];
    data && data.map((item, index) => {
        arr.push({ text: item, value: index });
    });
    return arr;
};
export const formatToSameField = (data) => {
    const arr = [];
    data && data.map((item) => {
        arr.push({ text: item, value: item });
    });
    return arr;
};

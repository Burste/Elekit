export const fetchData = (path, callback, config) => {
    const defaultConfig = {
        method: 'GET',
        header: {
            'X-XSRF-TOKEN': 'test',
        },
    };
    fetch(path, Object.assign({}, defaultConfig, config))
        .then((response) => {
            if (!response.ok) throw new Error(response.statusText);
            return response.json();
        })
        .then((data) => {
            callback(data);
        });
};
export const fetchMultiData = async (pathArr, callback) => {
    try {
        const [item1, item2, item3] = await Promise.all([
            fetch(pathArr[0]),
            fetch(pathArr[1]),
            fetch(pathArr[2]),
        ]);
        return [{ ...item1 }, { ...item2 }, { ...item3 }];
    } catch (err) {
        console.log('ERROR:' + err);
    }
};
export function fetchJsToObj(soure, callback) {
    fetch(soure, { method: 'GET' })
        .then((response) => response.text())
        .then((data) => {
            const newVariable = data.replace(/(?:var|let|const)\s(\w+)\s=/g, '"$1":').replace(/;/g, ',').replace(/,$/g, '').replace(/'/g, '"');
            return JSON.parse('{' + newVariable + '}');
        }).then((DataObj) => {
            callback(DataObj);
        });
}


export function findHighestZIndex(elem) {
    const elems = document.getElementsByTagName(elem);
    let highest = 0;
    for (let i = 0; i < elems.length; i++) {
        const zindex = document.defaultView.getComputedStyle(elems[i], null).getPropertyValue('z-index');
        if ((zindex > highest) && (zindex !== 'auto')) {
            highest = zindex;
        }
    }
    return parseInt(highest, 10);
}

export function getDomPosition(elem, property) {
    if (property === 'top') return elem.getBoundingClientRect()[property] + window.pageYOffset;
    return elem.getBoundingClientRect()[property];
}


// const utils = {
//     fetchJsToObj: fetchJsToObj,
//     findHighestZIndex: findHighestZIndex,
//     getDomPosition: getDomPosition,
//     toQueryString: toQueryString,
//     getYearAndMonth: getYearAndMonth,
//     getNowMonth: getNowMonth,
// };

// export { fetchJsToObj, findHighestZIndex, getDomPosition, toQueryString, getYearAndMonth, getNowMonth };
// export { utils as default };

export const verify = {
    checkMail: function(value) {
        if (!value) return '請輸入e-mail';
        const regExp = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        const Istype = regExp.test(value);
        if ( !Istype) return 'e-mail格式錯誤!';
        return '';
    },
    checkPsword: function(value) {
        if (!value) return '請輸入密碼';
        return '';
    },
    doubleCheckPsword: function(value, value2) {
        if (value !== value2) return '密碼不一致';
        return '';
    },
    agreeMemberTerm: function(value, value2) {
        if (value !== true) return '請勾選會員條款';
        return '';
    },
};
export class Verify {
    constructor(data, mode = 'default') {
        this.result = false;
        this.mode = mode;
        this.data = data;
        this.msg = {
            name: '', // 姓名
            email: '', // email
            classify: '', // 問題分類
            subject: '', // 主旨
            description: '', // 問題說明
        };
        // this.isEmpty = this.isEmpty.bind(this);
    }
    isEmpty = (str) => {
        return !!str;
    }
    checkEmail = (strEmail) => {
        const regExp = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        console.log('textEMAIL', strEmail);
        const Istype = regExp.test(strEmail);
        if (!strEmail) return '請填寫e-mail';
        if ( !Istype) return 'e-mail格式錯誤!';
        return '';
    }
    showData() {
        console.log(this.data);
        return this.data;
    }
    getGeneralCheckResult=()=>{
        const p = this.data;
        const msg = this.msg;
        // 姓名
        if (!p.name) msg.name = '請填寫姓名';
        // Email
        msg.email = this.checkEmail();
        // 問題分類：預設第一個選項 value=1
        if (!p.classify) msg.classify = '請選擇分類';
        // 主旨
        if (!p.subject) msg.subject = '請填寫主旨';
        // 問題說明
        if (!p.description) msg.description = '請填寫問題說明';
        this.result =
            Object.values(this.msg).reduce((obj, item) =>
                !!(obj || item), false);
        // if (!this.result) console.log('一切正常');
        return { status: !this.result, msg: this.msg };
    }
};

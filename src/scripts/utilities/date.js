
export function getYearAndMonth(dateStr) {
    let [year, month] = dateStr.split('-');
    year = Number(year);
    month = Number(month);
    return [year, month];
}

export function getNowMonth() {
    return new Date(new Date().getFullYear(), new Date().getMonth(), 1, 8).toISOString().slice(0, 7);
}

import actionTypes from './actionTypes';

// Add num
export const addNum = () => ({
    type: actionTypes.addNum,
    num: 1,
});

// Substract
export const subNum = () => ({
    type: actionTypes.subNum,
    num: 1,
});


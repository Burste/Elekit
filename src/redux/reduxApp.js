import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reducerCreator from './reducers';
// import { addNum } from './actions';
import { BtXin } from 'XinKit';
// todo state初始值
const initState = {
    total: 0,
    error: null,
};

// Create store
const store = createStore(
    reducerCreator(initState),
    // initState,
);
window.store = store;
class ReduxApp extends Component {
    static propTypes = {
        children: PropTypes.node,
    };
    componentDidMount() {
        // store.dispatch(addNum());
    }
    render() {
        return (
            <Provider store={store}>
                {this.props.children}
            </Provider>
        );
    }
}

export default ReduxApp;

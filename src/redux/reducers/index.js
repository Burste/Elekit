import actionTypes from '../actions/actionTypes';

const reducers = {
    [actionTypes.addNum]: function(state, action) {
        console.log('reducerADDNUM');
        return addNum(state, action);
    },
    [actionTypes.subNum]: function(state, action) {
        return subNum(state, action);
    },
};

export default function createReducers(initialState) {
    return function createReducers(state = initialState, action) {
        if (reducers.hasOwnProperty(action.type)) {
            return reducers[action.type](state, action);
        }
        return state;
    };
}

const addNum = (state, action) => {
    console.log('ADDNUM', state);
    return Object.assign({}, state, {
        total: state.total + action.num,
    });
};
const subNum = (state, action) => {
    return Object.assign({}, state, {
        total: state.total + action.num,
    });
};

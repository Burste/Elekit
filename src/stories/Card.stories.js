import React, { Component } from 'react';
import { storiesOf } from '@storybook/react';
import CdXmcc from '../components/Card/cd_xmcc/CdXmcc';
import LigXmside from '../components/List/lig_xmside/LigXmside';
import PropTypes from 'prop-types';

const arr =
{
    menu_title: '收到太多電子報',
    description: `歡迎您聯絡我們，讓我們為您取消欲退訂之電子報。
    溫馨提醒您，移除帳戶將影響您的訂單/文章管理/旅遊金權益。`,
    className: 'half',
};
const subarr =
{
    subMenu: [{
        title: '聯絡我們',
        className: 'bd_gray',
    },
    {
        title: '移除我們的帳戶',
        className: 'bd_gray',
    },
    ],
};

class DemoCdXmccComponet extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    static propTypes = {
        CdXmcc: PropTypes.func,
        SubComponent: PropTypes.func,
    };
    static defaultProps = {
    };
    render() {
        return (
            <div>
                <CdXmcc {...arr}
                    subComponent={(props)=><LigXmside parentState={props}
                        {...subarr} />} />
            </div>
        );
    }
}
storiesOf('Card', module).add('cd_xmcc', () => (
    <DemoCdXmccComponet />
));


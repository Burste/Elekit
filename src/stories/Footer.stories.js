import React from 'react';
import { storiesOf } from '@storybook/react';
import FtXin from '../components/Layout/Footer/ft_xin/FtXin';
import xinMember from '../images/xinMemberLogo.svg';

const logodata = {
    imgSrc: xinMember,
    link: '#',
    target: '',
};

const data = [
    {
        'menu_title': '欣會員',
        'subMenu': [
            {
                'title': '關於我們',
                'link': '#',
                'target': '',
            },
            {
                'title': '欣傳媒',
                'link': '#',
                'target': '',
            },
            {
                'title': '關於我們',
                'link': '#',
                'target': '',
            },
            {
                'title': '欣傳媒',
                'link': '#',
                'target': '',
            },
        ],
    },
    {
        'menu_title': '使用規章',
        'subMenu': [
            {
                'title': '使用條款',
                'link': '#',
                'target': '',
            },
            {
                'title': '常見問題',
                'link': '#',
                'target': '',
            },
        ],
    },
    {
        'menu_title': '客服專線',
        'subMenu': [
            {
                'title': '使用條款',
            },
            {
                'title': '常見問題',
                'link': '#',
                'target': '',
            },
        ],
    },
    {
        'menu_title': '客服專線',
        'subMenu': [
            {
                'title': '使用條款',
            },
            {
                'title': '常見問題',
                'link': '#',
                'target': '',
            },
        ],
    },
];


storiesOf('Footer', module).add('ft_xin', () => (
    <div>
        <FtXin
            logo={ logodata }
            fmenu={ data }
        />
    </div>
));


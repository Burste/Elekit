import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { storiesOf } from '@storybook/react';
import TbXin from '../components/Table/tb_xin/TbXin';
import TbPg from '../components/Table/tb_pg/TbPg';
import TableData from '../json/tableData.json';


class DemoTbPg extends Component {
    static propTypes = {
        defaultPage: PropTypes.number,
        defaultPageSize: PropTypes.number,
    };
    constructor(props) {
        super(props);
        this.state = {
            currentPage: props.defaultPage, // 目前顯示頁數(非輸入值)
        };
    }
    handlePageChange = (page, pageSize) => {
        // TODO: implement when create record page
        // 1. GET page & pageSize, render page
        this.setState((prevState) => ({
            currentPage: page,
        }));
        console.log(`${'page:' + page +
                       ',show:' + pageSize},${((page - 1) * pageSize) + 1}-${page * pageSize}`);
    }
    render() {
        return (
            <TbPg
                {...this.props}
                onChange={this.handlePageChange}
            />
        );
    }
}
storiesOf('Table', module).add('tb_xin', () => (
    <TbXin
        titleData={TableData.titleData}
        contentData={TableData.contentData}
    />
));
storiesOf('Table', module).add('tb_pg', () => (
    <DemoTbPg
        titleData={TableData.titleData}
        contentData={TableData.contentData}

        defaultPage={1} // 預設頁
        pageSize={20} // 頁面呈現數量
        total={60} // 資料數量
        // onChange={this.handlePageChange} // 換頁變更觸發
    />
));

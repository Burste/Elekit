import React, { Component } from 'react';
import { storiesOf } from '@storybook/react';
import PropTypes from 'prop-types';
import PgXin from '../components/Pagination/pg_xin/PgXin';

class DemoPgXin extends Component {
    static propTypes = {
        defaultPage: PropTypes.number,
        defaultPageSize: PropTypes.number,
    };
    static defaultProps = {
        defaultPage: 1,
    }
    constructor(props) {
        super(props);
        this.state = {
            currentPage: props.defaultPage, // 目前顯示頁數(非輸入值)
        };
    }
   handlePageChange = (page, pageSize) => {
       this.setState((prevState) => ({
           currentPage: page,
       }));
       console.log(`${'page:' + page +
                       ',show:' + pageSize},${((page - 1) * pageSize) + 1}-${page * pageSize}`);
   }
   render() {
       return (
           <PgXin
               defaultPage={1} // 預設頁
               pageSize={5} // 頁面呈現數量
               total={60} // 資料數量
               onChange={this.handlePageChange} // 換頁變更觸發
           />
       );
   }
}
storiesOf('Pagination', module).add('pg_xin', () => (
    <DemoPgXin
        // defaultPage={1} // 預設頁
        // pageSize={0} // 頁面呈現數量
        // total={60} // 資料數量
        // onChange={(page, pageSize)=>console.log(`${((page - 1) * pageSize) + 1}-${page * pageSize}`)} // 換頁變更觸發
    />
));
export default DemoPgXin;

import React from 'react';
import { storiesOf } from '@storybook/react';
import HdXmb from '../components/Layout/Header/hd_xmb/HdXmb';
import FtXin from '../components/Layout/Footer/ft_xin/FtXin';
import LayoutXin from '../components/Layout/Layout/layout_xin/LayoutXin';
import xinMember from '../images/xinMemberLogo.svg';
import xinUa from '../images/Xinua.png';
const logodata = {
    imgSrc: xinMember,
    link: '#',
    target: '',
};
const xinmenu = [
    {
        'title': '欣傳媒',
        'link': '#',
        'target': '',
    },
    {
        'title': '欣講堂',
        'link': '#',
        'target': '',
    },
    {
        'title': '欣嚴選',
        'link': '#',
        'target': '',
    },
];
const memberMenu = [
    {
        'menu_title': '個人帳戶管理',
        'subMenu': [
            {
                'title': '帳號資料設定',
                'link': '#',
                'target': '',
            },
        ],
    },
    {
        'menu_title': '旅遊金管理',
        'subMenu': [
            {
                'title': '旅遊金紀錄',
                'link': '#',
                'target': '',
            },
            {
                'title': '旅遊金說明',
                'link': '#',
                'target': '',
            },
        ],
    },
    {
        'menu_title': '訂單查詢',
        'subMenu': [
            {
                'title': '欣嚴選訂單',
                'link': '#',
                'target': '',
            },
            {
                'title': '欣圖庫訂單',
                'link': '#',
                'target': '',
            },
            {
                'title': '欣講堂訂單',
                'link': '#',
                'target': '',
            },
        ],
    },
    {
        'menu_title': '客服管道',
        'subMenu': [
            {
                'title': '會員條款',
                'link': '#',
                'target': '',
            },
            {
                'title': '聯絡我們',
                'link': '#',
                'target': '',
            },
        ],
    },
];
const headMembermenu = [];
memberMenu.forEach((item, i) =>{
    item.subMenu.forEach((mu, i) =>{
        headMembermenu.push(mu);
    });
});
headMembermenu.push(
    {
        'title': '登出',
        'link': '#',
        'target': '',
    },
);

const isLogin = {
    login: true,
    href: '#',
    name: 'Sam Lee Lee Sam Lee',
    data: {
        imgSrc: xinUa,
        link: '#',
        target: '',
    },
};
const footerData = [
    {
        'menu_title': '欣會員',
        'subMenu': [
            {
                'title': '關於我們',
                'link': '#',
                'target': '',
            },
            {
                'title': '欣傳媒',
                'link': '#',
                'target': '',
            },
            {
                'title': '關於我們',
                'link': '#',
                'target': '',
            },
            {
                'title': '欣傳媒',
                'link': '#',
                'target': '',
            },
        ],
    },
    {
        'menu_title': '使用規章',
        'subMenu': [
            {
                'title': '使用條款',
                'link': '#',
                'target': '',
            },
            {
                'title': '常見問題',
                'link': '#',
                'target': '',
            },
        ],
    },
    {
        'menu_title': '客服專線',
        'subMenu': [
            {
                'title': '使用條款',
            },
            {
                'title': '常見問題',
                'link': '#',
                'target': '',
            },
        ],
    },
    {
        'menu_title': '客服專線',
        'subMenu': [
            {
                'title': '使用條款',
            },
            {
                'title': '常見問題',
                'link': '#',
                'target': '',
            },
        ],
    },
];


storiesOf('Layout', module).add('layout_xin', () => (
    <LayoutXin
        header={
            <HdXmb
                isLogin={isLogin}
                logo={logodata}
                memberMenu={headMembermenu}
                xinMenu={xinmenu}
            />
        }
        footer={
            <FtXin
                logo={logodata}
                fmenu={footerData}
            />
        }
    >
        <span>123123</span>
    </LayoutXin>
));


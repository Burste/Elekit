import React, { Component } from 'react';
import { storiesOf } from '@storybook/react';
import LigXinmu from '../components/List/lig_xinmu/LigXinmu';
const xinmenu = [
    {
        'title': '欣傳媒',
        'link': '#',
        'target': '',
    },
    {
        'title': '欣講堂',
        'link': '#',
        'target': '',
    },
    {
        'title': '欣嚴選',
        'link': '#',
        'target': '',
    },
];

storiesOf('List', module).add('lig_xinmu', () => (
    <LigXinmu list={xinmenu} />
));


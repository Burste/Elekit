import React, { Component } from 'react';
import { storiesOf } from '@storybook/react';
import Stxin from '../components/Form/Select/St_xin/StXin';
import CdXmcc from '../components/Card/cd_xmcc/CdXmcc.js';
import BtXin from '../components/Button/bt_xin/BtXin.js';

class DemoStXin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mode: false,
        };
    }
    toggleContent = () => {
        this.setState((prevState) => ({
            mode: !this.state.mode,
        }));
    }

    render() {
        return (
            <div>
                <Stxin mode={this.state.mode}
                    toggleContent={this.toggleContent}
                    titleMode={
                        <BtXin text="v"
                            link="javascript:;"
                            className="bd_none"
                            onClick={this.toggleContent} />
                    }
                    content={<CdXmcc />}
                />
            </div>
        );
    }
}
storiesOf('Select', module).add('st_xin', () => (
    <DemoStXin />
));

import React from 'react';
import { storiesOf } from '@storybook/react';
import ClXmb from '../components/Carousel/cl_xmb/ClXmb';

const data = {
    imgSrc: ' http://fakeimg.pl/2000x900/?text=IMG',
    link: '',
    target: '',
};

storiesOf('Carousel', module).add('cl_xmb', () => (
    <ClXmb {...data} />
));

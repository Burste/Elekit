import React, { Component } from 'react';
import { storiesOf } from '@storybook/react';
import ClpXin from '../components/Collapse/clp_xin/ClpXin';
import CdXmcc from '../components/Card/cd_xmcc/CdXmcc';
import LigXmside from '../components/List/lig_xmside/LigXmside';
import PropTypes from 'prop-types';
// Example1 API
const arr = [
    {
        menu_title: '收到太多電子報',
        description: `歡迎您聯絡我們，讓我們為您取消欲退訂之電子報。
        溫馨提醒您，移除帳戶將影響您的訂單/文章管理/旅遊金權益。`,
        subMenu: [{
            title: '帳號資料設定',
            link: '#',
            target: '',
            className: 'bd_gray', // Api不會給 是否該重組字串!?
        },
        {
            title: '移除我們的帳戶',
            link: '#',
            target: '',
            className: 'bd_red',
        },
        ],
    },
    {
        menu_title: '我已經有另一個欣傳媒帳戶',
        description: `歡迎您聯絡我們，讓我們為您取消欲退訂之電子報。
        溫馨提醒您，移除帳戶將影響您的訂單/文章管理/旅遊金權益。`,
        subMenu: [{
            title: '聯絡我們',
            link: '#',
            target: '',
            className: 'bd_red',
        },
        ],
    },
    {
        menu_title: '我想使用其他 Email 登入帳戶',
        description: `歡迎您聯絡我們，讓我們為您取消欲退訂之電子報。
        溫馨提醒您，移除帳戶將影響您的訂單/文章管理/旅遊金權益。`,
        subMenu: [{
            title: '聯絡我們',
            link: '#',
            target: '',
            className: 'bd_gray',
        },
        {
            title: '移除我們的帳戶',
            link: '#',
            target: '',
            className: 'bd_red',
        },
        ],
    },
    {
        menu_title: '其他',
        description: `歡迎您聯絡我們，讓我們為您取消欲退訂之電子報。
        溫馨提醒您，移除帳戶將影響您的訂單/文章管理/旅遊金權益。`,
        subMenu: [{
            title: '聯絡我們',
            link: '#',
            target: '',
            className: 'bd_red',
        },
        ],
    },
];
// Example2 API
const arr1 = [
    {
        'menu_title': '個人帳戶管理',
        'subMenu': [
            {
                'title': '帳號資料設定',
                'link': '#',
                'target': '',
            },
        ],
    },
    {
        'menu_title': '旅遊金管理',
        'subMenu': [
            {
                'title': '旅遊金紀錄',
                'link': '#',
                'target': '',
            },
            {
                'title': '旅遊金說明',
                'link': '#',
                'target': '',
            },
        ],
    },
    {
        'menu_title': '訂單查詢',
        'subMenu': [
            {
                'title': '欣嚴選訂單',
                'link': '#',
                'target': '',
            },
            {
                'title': '欣圖庫訂單',
                'link': '#',
                'target': '',
            },
            {
                'title': '欣講堂訂單',
                'link': '#',
                'target': '',
            },
        ],
    },
    {
        'menu_title': '客服管道',
        'subMenu': [
            {
                'title': '會員條款',
                'link': '#',
                'target': '',
            },
            {
                'title': '聯絡我們',
                'link': '#',
                'target': '',
            },
        ],
    },
];

// Example1 DefaultStyle
const DefaultStyle = {
    type: 'radio',
    className: 'show',
    inputClassName: '',
    labelClassName: 'white',
};
// Example2 DefaultStyle
const DefaultStyle2 = {
    type: 'checkbox',
    className: 'titlewhite',
    inputClassName: '',
    labelClassName: 'white',
};

// Merge CdXmcc && LigXmside Componet
class mergeCdXmccComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    static propTypes = {
        CdXmcc: PropTypes.func,
        SubComponent: PropTypes.func,
    };
    static defaultProps = {
    };
    render() {
        return (
            <div>
                <CdXmcc {...this.props}
                    subComponent={(props)=><LigXmside {...this.props} />}
                />
            </div>
        );
    }
}

// HOC 1.Component 2.data
const Demo1 = new ClpXin(mergeCdXmccComponent, arr);
const Demo2 = new ClpXin(LigXmside, arr1);

// Demo
const DemoClpXinComponet = ()=>{
    return (
        <div>
            <h2>RADIO TYPE</h2>
            <h3>請問您欲移除帳戶的原因？</h3>
            <Demo1 defaultStyle={DefaultStyle}
                WrappedClassName='half' />
            <h2> CHECKBOX TYPE </h2>
            <Demo2 defaultStyle={DefaultStyle2}
                WrappedClassName='aside' />
        </div>
    );
};

storiesOf('Collapse', module).add('clp_xin', () => (
    <DemoClpXinComponet />
));


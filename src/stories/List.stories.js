import React, { Component } from 'react';
import { storiesOf } from '@storybook/react';
import LigXmside from '../components/List/lig_xmside/LigXmside';
// import PropTypes from 'prop-types';

const arr1 = {
    'subMenu': [
        {
            'title': '帳號資料設定',
            'link': '#',
            'target': '',
        },
    ],
};


class DemoLigXmsideComponet extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    static propTypes = {
    };
    static defaultProps = {
    };
    render() {
        return (
            <div>
                <LigXmside { ...arr1} />
            </div>
        );
    }
}

storiesOf('List', module).add('lig_xmside', () => (
    <DemoLigXmsideComponet />
));


import React, { Component } from 'react';
import { storiesOf } from '@storybook/react';
import IntXin from '../components/Form/Input/int_xin/IntXin.js';
import IntForm from '../components/Form/Input/int_form/IntForm.js';

const selectionData = [
    {
        type: 'text',
        placeholder: '請輸入文字',
        value: 'text here',
    },
    {
        type: 'text',
        placeholder: '請輸入文字',
    },
    {
        type: 'radio',
        placeholder: '請輸入文字',
        value: '12312312312',
    },
    {
        type: 'select',
        placeholder: '請輸入文字',
        value: '12312312312',
        selectionData: [
            { text: '1', value: 1 },
            { text: '2', value: 2 },
            { text: '3', value: 3 },
            { text: '4', value: 4 },
            { text: '5', value: 5 },
        ],
    },
    {
        type: 'radio',
        name: 'gender',
        selectionData: [
            { text: '男', value: 'M' },
            { text: '女', value: 'F' },
        ],
    },
    {
        type: 'checkbox',
        placeholder: '請輸入文字',
        value: '12312312312',
    },
    {
        type: 'labelText',
        value: 'S12312312312',
    },
];

const renderComponent = (arrData) =>{
    const arr = [];
    arrData && arrData.map((item, index)=>{
        arr.push(
            <div style={Object.assign({}, { margin: '10px 0', width: '300px' })}
                key={`demo_${index}`}
            >
                <IntXin {...item} />
            </div>
        );
    });
    return arr;
};

class DemoInputWithLabel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputValue: '',
        };
    }
    onChange = (e) => {
        const inputValue = e.target.value;
        console.log('CHANGE', inputValue);
        this.setState({
            inputValue: inputValue,
            radioValue: radioValue,
        });
    }
    render() {
        return (
            <React.Fragment>

                <IntForm
                    labelText="會員ID"
                    type="labelText"
                    value="S123456789"
                />
                <IntForm
                    labelText="姓名"
                    type="text"
                    placeholder="請輸入姓名"
                    value={this.state.inputValue}
                    // selectionData={selectionData}
                    msgErr={'請輸入姓名'}
                    onChange={(e)=>this.onChange(e)}
                />
                <IntForm
                    labelText="姓名"
                    type="text"
                    placeholder="請輸入姓名"
                    value={this.state.inputValue}
                    // selectionData={selectionData}
                    msgErr={''}
                    onChange={(e)=>this.onChange(e)}
                />
                <IntForm
                    labelText="性別"
                    type="radio"
                    name="gender"
                    value={this.state.radioValue}
                    selectionData={[{ text: 'Male', value: 'M' }, { text: 'Female', value: 'F' }]}
                    onChange={(e)=>this.onChange(e)}
                />
            </React.Fragment>
        );
    }
};
storiesOf('Input', module).add('int_xin', () => (
    renderComponent(selectionData)
));
storiesOf('Input', module).add('int_form', () => (
    <DemoInputWithLabel />
));

storiesOf('Input', module).add('int_rcln', () => (
    <DemoInputWithLabel />
));



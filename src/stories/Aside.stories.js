import React from 'react';
import { storiesOf } from '@storybook/react';
import AsideXin from '../components/Layout/Aside/aside_xin/AsideXin';
const list = [
    {
        'title': '個人訂單查詢',
        'subMenu': [
            {
                'title': '欣講堂訂單查詢',
                'link': '#',
                'target': 'F',
            },
            {
                'title': '欣嚴選訂單查詢',
                'link': '#',
                'target': 'F',
            },
        ],
    },
    {
        'title': '文章管理',
        'subMenu': [
            {
                'text': '品牌管理',
                'link': '#',
                'target': 'F',
            },
            {
                'text': '發表文章',
                'link': '#',
                'target': 'F',
            },
            {
                'text': '我的通知',
                'link': '#',
                'target': 'F',
            },
            {
                'text': '訂閱管理',
                'link': '#',
                'target': 'F',
            },
        ],
    },
    {
        'title': '旅遊金',
        'subMenu': [
            {
                'text': '旅遊金商城',
                'link': '#',
                'target': 'F',
            },
            {
                'text': '旅遊金查詢',
                'link': '#',
                'target': 'F',
            },
            {
                'text': '兌換紀錄',
                'link': '#',
                'target': 'F',
            },
        ],
    },
    {
        'title': '客服管道',
        'className': 'inline',
        'subMenu': [
            {
                'text': '聯絡我們',
                'link': '#',
                'target': 'F',
            },
            {
                'text': '常見FAQ',
                'link': '#',
                'target': 'F',
            },
            {
                'text': '會員條款',
                'link': '#',
                'target': 'F',
            },
            {
                'text': '線上客服',
                'link': '#',
                'target': 'F',
            },
        ],
    },
    {
        'title': '登出',
        'link': '#',
        'target': '',
    },
];
storiesOf('Aside', module).add('aside_xin', () => (
    <AsideXin list={list} />
));


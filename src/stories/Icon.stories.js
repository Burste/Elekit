import React from 'react';
import { storiesOf } from '@storybook/react';
import IcXin from '../components/Icon/ic_xin/IcXin.js';
const showstyle = `.icon_lists li{
    float:left;
    width: 150px;
    height:180px;
    text-align: center;
  }
  .icon_lists{
    list-style-type: none;
  }
  .icon_lists .ic_xin{
    font-size: 42px;
    line-height: 100px;
    margin: 10px 0;
    color:#333;
    -webkit-transition: font-size 0.25s ease-out 0s;
    -moz-transition: font-size 0.25s ease-out 0s;
    transition: font-size 0.25s ease-out 0s;
  }
  .icon_lists .ic_xin:hover{
    font-size: 100px;
  }`;

const sheet = document.createElement('style');
sheet.innerHTML = showstyle;
document.body.appendChild(sheet);
storiesOf('Icon', module).add('ic_xin', () => (
    <div>
        <ul className={`icon_lists clear`}>
            <li>
                <IcXin name="ic-arrow-down"
                    size="x15" />
                <div className="name">ic-arrow-down</div>
                <div className="code">&amp;#xe900;</div>
                <div className="name">.ic-arrow-down</div>
            </li>
            <li>
                <IcXin name="ic-back" size="x15" />
                <div className="name">ic-back</div>
                <div className="code">&amp;#xe901;</div>
                <div className="name">.ic-back</div>
            </li>
            <li>
                <IcXin name="ic-bell" size="x15" />
                <div className="name">ic-bell</div>
                <div className="code">&amp;#xe902;</div>
                <div className="name">.ic-bell</div>
            </li>
            <li>
                <IcXin name="ic-cart" size="x15" />
                <div className="name">ic-cart</div>
                <div className="code">&amp;#xe903;</div>
                <div className="name">.ic-cart</div>
            </li>
            <li>
                <IcXin name="ic-cross" size="x15" />
                <div className="name">ic-cross</div>
                <div className="code">&amp;#xe904;</div>
                <div className="name">.ic-cross</div>
            </li>
            <li>
                <IcXin name="ic-delete" size="x15" />
                <div className="name">ic-bell</div>
                <div className="code">&amp;#xe905;</div>
                <div className="name">.ic-delete</div>
            </li>
            <li>
                <IcXin name="ic-fb" size="x15" />
                <div className="name">ic-fb</div>
                <div className="code">&amp;#xe906;</div>
                <div className="name">.ic-fb</div>
            </li>
            <li>
                <IcXin name="ic-filter" size="x15" />
                <div className="name">ic-filter</div>
                <div className="code">&amp;#xe908;</div>
                <div className="name">.ic-filter</div>
            </li>
            <li>
                <IcXin name="ic-google" size="x15" />
                <div className="name">ic-google</div>
                <div className="code">&amp;#xe909;</div>
                <div className="name">.ic-google</div>
            </li>
            <li>
                <IcXin name="ic-hot" size="x15" />
                <div className="name">ic-hot</div>
                <div className="code">&amp;#xe90b;</div>
                <div className="name">.ic-hot</div>
            </li>
            <li>
                <IcXin name="ic-like" size="x15" />
                <div className="name">ic-like</div>
                <div className="code">&amp;#xe90c;</div>
                <div className="name">.ic-like</div>
            </li>
            <li>
                <IcXin name="ic-line" size="x15" />
                <div className="name">ic-line</div>
                <div className="code">&amp;#xe90d;</div>
                <div className="name">.ic-line</div>
            </li>
            <li>
                <IcXin name="ic-menu" size="x15" />
                <div className="name">ic-menu</div>
                <div className="code">&amp;#xe90f;</div>
                <div className="name">.ic-menu</div>
            </li>
            <li>
                <IcXin name="ic-message" size="x15" />
                <div className="name">ic-message</div>
                <div className="code">&amp;#xe910;</div>
                <div className="name">.ic-message</div>
            </li>
            <li>
                <IcXin name="ic-point" size="x15" />
                <div className="name">ic-point</div>
                <div className="code">&amp;#xe911;</div>
                <div className="name">.ic-point</div>
            </li>
            <li>
                <IcXin name="ic-search" size="x15" />
                <div className="name">ic-search</div>
                <div className="code">&amp;#xe912;</div>
                <div className="name">.ic-search</div>
            </li>
            <li>
                <IcXin name="ic-seat" size="x15" />
                <div className="name">ic-seat</div>
                <div className="code">&amp;#xe913;</div>
                <div className="name">.ic-seat</div>
            </li>
        </ul>
    </div>
));

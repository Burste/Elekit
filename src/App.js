import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import Container from './redux/ReduxApp';
import { BtXin } from 'XinKit';

const Frame = styled.div`
    width: 400px;
`;
class App extends Component {
    render() {
        return (
            <Container>
                <Frame>
                    <BtXin
                        className="bd_red"
                        text="按鈕"
                    />
                </Frame>
            </Container>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('root'));
